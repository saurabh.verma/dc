<?php

session_start();
ob_start();
include 'data/config.php';
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Data Collect Admin Panel</title>

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-img3-body">

    <div class="container">

      <form class="login-form" method="post">
        <div class="login-wrap">
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="text" class="form-control" placeholder="Username" id="username"  name="username" autofocus required>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
            </div>
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
            </label>
            <button class="btn btn-primary btn-lg btn-block" type="submit" id="login" name="login">Login</button>

        </div>
      </form>

      <?php
      if(isset($_POST['login']))
      {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $hash = md5($password);


        $checkuser = "SELECT * FROM usermaster WHERE username = '$username' AND hashpassword = '$hash'";
        $querycheck = mysqli_query($conn,$checkuser);
        $userow = mysqli_num_rows($querycheck);
        $checkdata = mysqli_fetch_assoc($querycheck);
      //  print_r($checkdata);
        if($userow>0)
        {

          $_SESSION['logged_in'] = $checkdata;
         // print_r($_SESSION['logged_in']);
          if ($_SESSION['logged_in']['usertype']==1) {
              header("Location:panel/dash.php");
          }
          else
         {
             
              header("Location:employee/add.php");
             
         }
            

        }
        else {
          echo "<script>alert('please check username and password')</script>";
        }

      }

       ?>
    <div class="text-right">
            <div class="credits">
                <!--
                    All the links in the footer should remain intact.
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
                -->
                Designed and Developed by</a> by <a href="http://illywhackertechnologies.com/index.html">Illywhacker Technologies Pvt. Ltd.</a>
            </div>
        </div>
    </div>


  </body>
</html>
