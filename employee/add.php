<?php
ob_start();
ini_set('max_execution_time', 300);
include '../header.php'; ?>

  <body>
  <!-- container section start -->
  <section id="container" class="">


    <?php include '../topbar.php'; ?>
      <!--header end-->

      <!--sidebar start-->
    <?php include '../leftbar.php';
    ?>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="username"> Welcome <?php  print_r($_SESSION['logged_in']['username']); ?></h3>

        <h3 class="page-header"><i class="fa fa-files-o"></i> Add Staff</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="../panel/dash.php">Home</a></li>
          <li><i class="icon_document_alt"></i>Add</li>
          <li><i class="fa fa-files-o"></i>Staff</li>
        </ol>
      </div>
    </div>

            <!-- Form validations -->
            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Form Tittle</h4>
                                    </div>
                                    <div class="modal-body">

                                        <!-- <form role="form" method="post"> -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">City Name</label>
                                                <input type="email" class="form-control" id="cityname" name="cityname" placeholder="Enter City Name">
                                            </div>


                                            <button type="submit" class="btn btn-primary" id="csave" name="csave">Save</button>
                                        <!-- </form> -->
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-2" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                    <h4 class="modal-title">Form Tittle</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <!-- <form role="form" method="post"> -->
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Department Name</label>
                                                            <input type="email" class="form-control" id="dname" name="dname" placeholder="Enter Department Name">
                                                        </div>


                                                        <button type="submit" class="btn btn-primary" id="csaves" name="csaves">Save</button>
                                                    <!-- </form> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                           Add Staff
                        </header>
                        <?php if ($_SESSION['logged_in']['usertype']==1) {
                        echo '  <a href="#myModal-1" data-toggle="modal" class="btn  btn-primary" style="  margin-left: 84px;  margin-top: 14px;">
                                    Add City
                                </a>
                                <a href="#myModal-2" data-toggle="modal" class="btn  btn-primary" style="  margin-left: 84px;  margin-top: 14px;">
                                            Add Department
                                        </a>';
                        } ?>

                        <div class="panel-body">
                            <div class="form">
                                <form class="form-validate form-horizontal " id="register_form" method="post">
                                    <div class="form-group ">
                                        <label for="fullname" class="control-label col-lg-2">Full name <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="fullname" name="fullname" type="text"  required/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="email" class="control-label col-lg-2">Email <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="email" name="email" type="email" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="mobile" class="control-label col-lg-2">Mobile No<span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="mobile" name="mobile" type="text"  required maxlength="10" value="" />
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label for="username" class="control-label col-lg-2">Username <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="username" name="username" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="password" class="control-label col-lg-2">Password <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="password" name="password" type="password"  required/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="city" class="control-label col-lg-2">Employee City <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                          <select class="form-control m-bot15" id="city" name="city" >
                                            <option value="">--Please Select Employee City</option>
                                            <?php
                                            if ($_SESSION['logged_in']['usertype']==1)
                                            {
                                              $employe = "SELECT `id`, `cityname` FROM `city` WHERE delid =0";
                                              $equery = mysqli_query($conn,$employe);

                                              while ($edata = mysqli_fetch_assoc($equery)) {
                                              echo "  <option value=".$edata['id'].">".$edata['cityname']."</option> ";
                                              }
                                            }
                                            else {
                                              $city = $_SESSION['logged_in']['usercity'];
                                              $employe = "SELECT `id`, `cityname` FROM `city` WHERE delid =0 and `id` = '$city'";
                                              $equery = mysqli_query($conn,$employe);

                                              while ($edata = mysqli_fetch_assoc($equery)) {
                                              echo "  <option value=".$edata['id'].">".$edata['cityname']."</option> ";
                                              }
                                            }

                                           ?>

                                        </select>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="etype" class="control-label col-lg-2">Employee Type <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                          <select class="form-control m-bot15" id="etype" name="etype" >
                                            <option value="">--Please Select Employee Type</option>
                                            <?php
                                            if ($_SESSION['logged_in']['usertype']==1)
                                            {
                                              $employe = "SELECT `id`, `ename` FROM `employee` WHERE delid =0";
                                              $equery = mysqli_query($conn,$employe);

                                              while ($edata = mysqli_fetch_assoc($equery)) {
                                              echo "  <option value=".$edata['id'].">".$edata['ename']."</option> ";
                                              }
                                            }
                                            else {
                                              $city = $_SESSION['logged_in']['usercity'];
                                              $employe = "SELECT `id`, `ename` FROM `employee` WHERE delid =0 and id = 3" ;
                                              $equery = mysqli_query($conn,$employe);

                                              while ($edata = mysqli_fetch_assoc($equery)) {
                                              echo "  <option value=".$edata['id'].">".$edata['ename']."</option> ";
                                              }
                                              }


                                           ?>


                                        </select>
                                        </div>
                                    </div>
                                    <input id="usertype" hidden value="<?php echo $_SESSION['logged_in']['usertype'];?>">
                                      <input id="userid" hidden value="<?php echo $_SESSION['logged_in']['id'];?>">
                                    <?php
                                    if ($_SESSION['logged_in']['usertype']==1)
                                    {
                                  echo'  <div class="form-group " hidden id="manangername">
                                        <label for="etype" class="control-label col-lg-2">Executive Manager <span class="required">*</span></label>
                                        <div class="col-lg-10">';

                                          echo '  <select class="form-control m-bot15" id="mname" name="mname" >
                                              <option value="">--Please Select Manager Name </option>';


                                      echo'  </select>
                                        </div>
                                    </div>';
                                  }
                                  else {
                                    echo'  <div class="form-group " hidden id="manangername2">
                                          <label for="etype" class="control-label col-lg-2">Executive Manager <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                          <input  hidden value="'.$_SESSION['logged_in']['id'].'" id="mname2" name="mname2" >';

                                        //   $userid = $_SESSION['logged_in']['id'];
                                        //   $employe = "SELECT `id`, `fullname` FROM `usermaster` WHERE delid = 0 and id = $userid" ;
                                        //   $equery = mysqli_query($conn,$employe);
                                        //   echo '  <select class="form-control m-bot15" id="mname2" name="mname2" >
                                        //       <option value="">--Please Select Manager Name </option>';
                                        //
                                        //   while ($edata = mysqli_fetch_assoc($equery)) {
                                        //   echo "  <option value=".$edata['id'].">".$edata['fullname']."</option> ";
                                        //   }
                                        //
                                        //
                                        // echo'  </select>
                                        echo'  </div>
                                      </div>';





                                  }
                                  ?>
                                    <div class="form-group " hidden id="worktype">
                                        <label for="etype" class="control-label col-lg-2">Executive Department <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                          <select class="form-control m-bot15" id="wname" name="wname" >
                                            <option value="">--Please Select Executive Department </option>
                                            <?php
                                            $employe = "SELECT `id`, `workname` FROM `work` WHERE delid =0";
                                            $equery = mysqli_query($conn,$employe);

                                            while ($edata = mysqli_fetch_assoc($equery)) {
                                            echo "  <option value=".$edata['id'].">".$edata['workname']."</option> ";
                                            } ?>


                                        </select>
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <div class="col-lg-offset-2 col-lg-10" id="managersave">
                                            <button  class="btn btn-primary" id="usersave" name="usersave" type="submit">Save</button>


                                        </div>
                                        <div class="col-lg-offset-2 col-lg-10" hidden id="executivesave">
                                            <button  class="btn btn-primary" id="usersaved" name="usersaved" type="submit">Save</button>

                                        </div>
                                    </div>
                                </form>
                            </div>
                          </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
          <div class="text-right">
          <div class="credits">
                <!--
                    All the links in the footer should remain intact.
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
                -->
                <a href="#"> </a> Powered by <a href="http://www.illywhackertechnologies.com/">Illywhackertechnologies.com</a>
            </div>
        </div>
      </section>
      <!--main content end-->
  </section>

  <!-- container section start -->
  <?php
  include '../footer/add_footer.php';
   ?>
