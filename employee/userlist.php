
<?php
ini_set('max_execution_time', 300);
include '../header.php'; ?>

  <body>
  <!-- container section start -->
  <section id="container" class="">


    <?php include '../topbar.php'; ?>
      <!--header end-->

      <!--sidebar start-->
    <?php include '../leftbar.php';
    ?>
      <!--sidebar end-->

      <!--main content start-->
      <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">

                                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                  <h4 class="modal-title">Form Tittle</h4>
                              </div>
                              <div class="modal-body">

                                  <!-- <form role="form" method="post"> -->
                                      <div class="form-group">
                                        <input id="id"  hidden>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Full Name</label>
                                            <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Enter New Password ">
                                        </div>
                                          <label for="exampleInputEmail1"> User Name</label>
                                          <input type="email" class="form-control" id="username" name="username" placeholder="Enter User Name">
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputEmail1"> Password</label>
                                          <input type="email" class="form-control" id="password" name="password" placeholder="Enter New Password ">
                                      </div>

                                      <div class="form-group">
                                          <label for="exampleInputEmail1"> Mobile No:</label>
                                          <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Enter New Password ">
                                      </div>
                                      <div class="form-group">
                                          <label for="exampleInputEmail1"> Email</label>
                                          <input type="text" class="form-control" id="email" name="email" placeholder="Enter New Password ">
                                      </div>



                                      <button type="submit" class="btn btn-primary" id="esave" name="esave">Save</button>
                                  <!-- </form> -->
                              </div>
                          </div>
                      </div>
                  </div>
      <section id="main-content">
        <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <h3 class="username"> Welcome <?php  print_r($_SESSION['logged_in']['username']); ?></h3>
        <h3 class="page-header"><i class="fa fa-table"></i> Manager </h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
          <li><i class="fa fa-table"></i>Staff</li>
          <li><i class="fa fa-th-list"></i> Manager</li>
        </ol>
      </div>
    </div>
            <!-- page start-->



<div class="row">
  <div class="form-group ">

      <div class="col-md-6">
          <label for="etype" class="control-label col-md-1"> City </label>
        <select class="form-control m-bot15" id="city" name="city" >
          <option value="">--Please Select Employee City</option>
          <?php
          $employe = "SELECT `id`, `cityname` FROM `city` WHERE delid =0";
          $equery = mysqli_query($conn,$employe);

          while ($edata = mysqli_fetch_assoc($equery)) {
          echo "  <option value=".$edata['id'].">".$edata['cityname']."</option> ";
          } ?>

      </select>
      </div>
      <div class="col-md-6">
        <label for="etype" class="control-label col-md-1">Employee </label>
        <select class="form-control m-bot15" id="etype" name="etype" >
          <option value="">--Please Select Employee Type</option>
          <?php
          $employe = "SELECT `id`, `ename` FROM `employee` WHERE delid =0 and (id = 1 or id = 2)";
          $equery = mysqli_query($conn,$employe);

          while ($edata = mysqli_fetch_assoc($equery)) {
          echo "  <option value=".$edata['id'].">".$edata['ename']."</option> ";
          } ?>

      </select>
      </div>
  </div>

</div>
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel" id="newtable">

                        <header class="panel-heading">
                            Advanced Table
                        </header>

                        <table id="example" class="table">
  			<thead>
        <tr>
            <th>S.No</th>
          <th>Full Name</th>
          <th>UserName</th>
          <th>Password</th>
            <th>Contact</th>
              <th>City</th>
                <th>Designation</th>
                <th>Optios</th>

        </tr>
      </thead>
      <tbody>
        <?php
        $userlist = "SELECT usermaster.id as uid,usermaster.username as username,usermaster.fullname as fullname,usermaster.email as email,usermaster.mobile as mobile,usermaster.password as password,employee.ename as emplpoyetype ,city.cityname as cityname FROM usermaster JOIN employee,city WHERE usermaster.delid = 0 AND usermaster.usertype = employee.id AND usermaster.usercity= city.id and city.delid = 0";
        $userquery = mysqli_query($conn,$userlist);
        $i = 1;
        while ($data = mysqli_fetch_assoc($userquery)) {
          $id = $data['uid'];
        echo '<tr>
          <td>'.$i.'</td>
          <td>'.$data['fullname'].'</td>
          <td>'.$data['username'].'</td>
          <td>'.$data['password'].'</td>
          <td>Email Id-'.$data['email'].'<br>Mobile-'.$data['mobile'].'</td>
          <td>'.$data['cityname'].'</td>
          <td>'.$data['emplpoyetype'].'</td>
          <td> <a href="#myModal-1" data-toggle="modal"  data-id="'.$id.'" class=" edit">
                      Edit
                  </a>
          <br>
          <a href="#"  class="delete"  data-id="'.$id.'" style="color: red;" >Delete</a></td>

        </tr>';
        $i++;
        } ?>


      </tbody>
    </table>
  		                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
          <div class="text-right">
          <div class="credits">
                <!--
                    All the links in the footer should remain intact.
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
                -->
  <a href="#"> </a> Powered by <a href="http://www.illywhackertechnologies.com/">Illywhackertechnologies.com</a>            </div>
        </div>
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->
  <?php
  include '../footer/userlist_footer.php'; ?>
>
