
    <!-- javascripts -->
    <!-- <script src="../js/jquery.js"></script> -->
	<script src="../js/jquery-ui-1.10.4.min.js"></script>
    <!-- <script src="../js/jquery-1.8.3.min.js"></script> -->
    <script type="text/javascript" src="../js/jquery-ui-1.9.2.custom.min.js"></script>
    <!-- bootstrap -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="../js/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../js/jquery.validate.min.js"></script>
    <script src="../js/jquery.nicescroll.js" type="text/javascript"></script>
    <!-- charts scripts -->
    <script src="../assets/jquery-knob/js/jquery.knob.js"></script>
    <script src="../js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="../assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="../js/owl.carousel.js" ></script>
    <!-- jQuery full calendar -->
    <<script src="../js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
	<script src="../assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <!--script for this page only-->
    <script src="../js/calendar-custom.js"></script>
	<script src="../js/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script src="../js/jquery.customSelect.min.js" ></script>
	<script src="../assets/chart-master/Chart.js"></script>

    <!--custome script for all page-->
      <script src="../js/form-validation-script.js"></script>
    <script src="../js/scripts.js"></script>
    <!-- custom script for this page-->
    <script src="../js/sparkline-chart.js"></script>
    <script src="../js/easy-pie-chart.js"></script>
	<script src="../js/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="../js/jquery-jvectormap-world-mill-en.js"></script>
	<script src="../js/xcharts.min.js"></script>
	<script src="../js/jquery.autosize.min.js"></script>
	<script src="../js/jquery.placeholder.min.js"></script>
	<script src="../js/gdp-data.js"></script>
	<script src="../js/morris.min.js"></script>
	<script src="../js/sparklines.js"></script>
	<script src="../js/charts.js"></script>
	<script src="../js/jquery.slimscroll.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script>
	$(document).ready(function(){
    $('#example').DataTable();
});
</script>
  <script>

      //knob
      $(function() {
        $(".knob").knob({
          'draw' : function () {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
          $("#owl-slider").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

	  /* ---------- Map ---------- */
	$(function(){
	  $('#map').vectorMap({
	    map: 'world_mill_en',
	    series: {
	      regions: [{
	        values: gdpData,
	        scale: ['#000', '#000'],
	        normalizeFunction: 'polynomial'
	      }]
	    },
		backgroundColor: '#eef3f7',
	    onLabelShow: function(e, el, code){
	      el.html(el.html()+' (GDP - '+gdpData[code]+')');
	    }
	  });
	});

  </script>

  <script>
  $(document).ready(function(){
		$("#logout").click(function() {
		  //  var addressValue = $(this).attr("id");

			var userid = $(this).attr("data-userid");
		      //  alert(oid
		      //  alert(userid);
			$.ajax({
		           type: 'POST',
		        url: '../Ajax/logout.php',
		           data: {  'userid': userid},
		          success: function(resp){
		     alert(resp);
		     window.location.href = "../index.php";

			 //location.reload();
		   }
		 });

		});
		$('.delete').click(function(e){

		 e.preventDefault();

		 var element = $(this);
		 var id = $(element).data('id');


 //alert(id);
 $.ajax({
					 type: 'POST',
					 url: '../Ajax/del_d.php',
					 data: {'id': id,},
					 success: function(resp){
						 alert(resp);

							 location.reload();


	}
 });
		});

		$('.edit').click(function(e){

		 e.preventDefault();

		 var element = $(this);
		 var id = $(element).data('id');



 alert(id);
 // $.ajax({
 // 				 type: 'POST',
 // 				 url: '../Ajax/edit_job.php',
 // 				 data: {'id': id,},
 // 				 success: function(resp){
 // 					 //alert(resp);
 // 							 var data = JSON.parse(resp);
 // 								 $.each(data, function (i, o){
 // 									 document.getElementById("euserid").value = o.id;
 // 										 document.getElementById("elink").value = o.link;
 // 											 document.getElementById("eurl").value = o.url;
 // 												 document.getElementById("edetails").value = o.details;
 // 												 document.getElementById("epoints").value = o.points;
 //
 //
 // 								 });
 // }
 // });
		});
  $("#city").change(function(){


  var id = $("#city").val();
  //alert(id);



  // Returns successful data submission message when the entered information is stored in database.
  var dataString = 'id='+ id+'&type=1';
  if(id=='')
  {
  alert("Please Fill City Name ");
  }
  else
  {
  //alert(dataString);
  // AJAX Code To Submit Form.
  $.ajax({
  type: "POST",
  url: "../Ajax/usertable.php",
  data: dataString,
  cache: false,
  success: function(result){
  //  alert(result);
    $("#newtable").html(result);

  }
  });
  }
  return false;
  });

  //type
  $("#etype").change(function(){


  var id = $("#etype").val();
  //alert(id);



  // Returns successful data submission message when the entered information is stored in database.
  var dataString = 'id='+ id+'&type=2';
  if(id=='')
  {
  alert("Please Fill City Name ");
  }
  else
  {
  //alert(dataString);
  // AJAX Code To Submit Form.
  $.ajax({
  type: "POST",
  url: "../Ajax/usertable.php",
  data: dataString,
  cache: false,
  success: function(result){
  //  alert(result);
$("#newtable").html(result);

  }
  });
  }
  return false;
  });
  });
  </script>
<script>
$(document).ready(function(){
$('#city').change(updateFormEnabled);
$('#etype').change(updateFormEnabled);

});

$(document).ready(function(){
$("#csave").click(function(){
var cityname = $("#cityname").val();

// Returns successful data submission message when the entered information is stored in database.
var dataString = 'cityname='+ cityname+'&type=1' ;
if(cityname=='')
{
alert("Please Fill City Name ");
}
else
{
//alert(dataString);
// AJAX Code To Submit Form.
$.ajax({
type: "POST",
url: "../Ajax/data.php",
data: dataString,
cache: false,
success: function(result){

if(result==200)
{
  alert('successfully Added');
  location.reload();
}
else {
  alert(result);
}

}
});
}
return false;
});

//department save

$("#csaves").click(function(){
var cityname = $("#dname").val();

// Returns successful data submission message when the entered information is stored in database.
var dataString = 'dname='+ cityname+'&type=2' ;
if(cityname=='')
{
alert("Please Fill City Name ");
}
else
{
//alert(dataString);
//AJAX Code To Submit Form.
$.ajax({
type: "POST",
url: "../Ajax/data.php",
data: dataString,
cache: false,
success: function(result){

if(result==200)
{
  alert('successfully Added');
  location.reload();
}
else {
  alert(result);
}

}
});
}
return false;
});
});
</script>
     </body>
   </html>
