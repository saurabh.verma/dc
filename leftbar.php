<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
              <?php if ($_SESSION['logged_in']['usertype']==1) {
                  echo ' <li class="active">
                <a class="" href="../panel/dash.php">
                    <i class="icon_house_alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>';
              }
              ?>

    <li class="sub-menu">
                <a href="../employee/add.php" class="">
                    <i class="icon_document_alt"></i>
                    <?php if ($_SESSION['logged_in']['usertype']==1) {
                      echo '  <span>Add Staff</span>';
                    }
                    else {
                      echo '  <span>Add Executive</span>';
                    }?>

                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <!-- <ul class="sub">
                    <li><a class="" href="form_component.html">Form Elements</a></li>
                    <li><a class="" href="form_validation.html">Form Validation</a></li>
                </ul> -->
            </li>
            <?php if ($_SESSION['logged_in']['usertype']==1) {
            echo '   <li class="sub-menu">
                  <a href="../employee/userlist.php" class="">
                      <i class="icon_desktop"></i>
                      <span>Manager List</span>
                      <span class="menu-arrow arrow_carrot-right"></span>
                  </a>
                  <!-- <ul class="sub">
                      <li><a class="" href="../employee/userlist.php">Manager</a></li>

                  </ul> -->
              </li>';
            } ?>

            <li class="sub-menu">
                <a href="../employee/executive.php" class="">
                    <i class="icon_desktop"></i>
                    <span>Executive List</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <!-- <ul class="sub">
                    <li><a class="" href="../employee/userlist.php">Manager</a></li>

                </ul> -->
            </li>
            <li>
                <a class="" href="../panel/addjob.php">
                    <i class="icon_genius"></i>
                    <span>Add Job</span>
                </a>
            </li>
             <li class="sub-menu">
                <a href="../panel/alljobs.php">
                    <i class="icon_piechart"></i>
                    <span>Assigned Jobs </span>

                </a>

            </li>
            <li>
                <a class="" href="../panel/job_report.php">
                    <i class="icon_piechart"></i>
                    <span>Job Report</span>

                </a>

            </li>



            <li class="sub-menu">
                <a href="../panel/reports.php">
                    <i class="icon_table"></i>
                    <span>Daily Report</span>

                </a>

            </li>
            <?php if ($_SESSION['logged_in']['usertype']==1) {
            echo '  <li class="sub-menu">
                <a href="../panel/area.php">
                    <i class="icon_documents_alt"></i>
                    <span>Service Area</span>

                </a>

            </li>
            <li class="sub-menu">
                <a href="../panel/department.php">
                    <i class="icon_documents_alt"></i>
                    <span>Departments </span>

                </a>

            </li>

            ';
            } ?>



            <!-- <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_documents_alt"></i>
                    <span>Pages</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="profile.html">Profile</a></li>
                    <li><a class="" href="login.html"><span>Login Page</span></a></li>
                    <li><a class="" href="blank.html">Blank Page</a></li>
                    <li><a class="" href="404.html">404 Error</a></li>
                </ul>
            </li> -->

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
