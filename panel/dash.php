<?php
ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '100M');
ini_set('max_input_time', 300);
ini_set('max_execution_time', 300);

include '../header.php'; ?>

  <body>
  <!-- container section start -->
  <section id="container" class="">


    <?php include '../topbar.php'; ?>
      <!--header end-->

      <!--sidebar start-->
    <?php include '../leftbar.php';
    ?>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!--overview start-->
			  <div class="row">
				<div class="col-lg-12">
            <h3 class="username"> Welcome <?php  print_r($_SESSION['logged_in']['username']); ?></h3>
					<h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="#">Home</a></li>
						<li><i class="fa fa-laptop"></i>Dashboard</li>
					</ol>
				</div>
			</div>

            <div class="row">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="info-box blue-bg">
						<i class="fa fa-cloud-download"></i>
            <?php
            $manager = "SELECT COUNT(id) as id FROM usermaster WHERE delid =0 AND usertype !=1";
            $managerquery = mysqli_query($conn,$manager);
            $mdata = mysqli_fetch_assoc($managerquery); ?>
						<div class="count"><?php print_r($mdata['id']); ?></div>
						<div class="title"> Total Manager</div>
					</div><!--/.info-box-->
				</div><!--/.col-->

				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="info-box brown-bg">
						<i class="fa fa-shopping-cart"></i>
            <?php
            $manager = "SELECT COUNT(id) as id FROM executive WHERE delid = 0";
            $managerquery = mysqli_query($conn,$manager);
            $mdata = mysqli_fetch_assoc($managerquery); ?>
						<div class="count"><?php print_r($mdata['id']); ?></div>
						<div class="title"> Total Executive </div>
					</div><!--/.info-box-->
				</div><!--/.col-->

				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="info-box dark-bg">
						<i class="fa fa-thumbs-o-up"></i>
            <?php
            $manager = "SELECT COUNT(id) as id FROM city WHERE delid =0 ";
            $managerquery = mysqli_query($conn,$manager);
            $mdata = mysqli_fetch_assoc($managerquery); ?>
            <div class="count"><?php print_r($mdata['id']); ?></div>
            <div class="title"> Total Area </div>
					</div><!--/.info-box-->
				</div><!--/.col-->

				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="info-box green-bg">
						<i class="fa fa-cubes"></i>
            <?php
            $manager = "SELECT COUNT(id) as id FROM newjob WHERE delid = 0 ";
            $managerquery = mysqli_query($conn,$manager);
            $mdata = mysqli_fetch_assoc($managerquery);
             $date = date('d/m/Y');
            $managers = "SELECT COUNT(id) as id FROM newjob WHERE delid = 0 and date = '$date' ";
            $managerquerys = mysqli_query($conn,$managers);
            $mdatas = mysqli_fetch_assoc($managerquerys);?>
            <div class="count"><?php print_r($mdata['id']); ?></div>
            <div class="title"><?php print_r($mdatas['id']); ?></div>
					</div><!--/.info-box-->
				</div><!--/.col-->

			</div><!--/.row-->




		  <!-- Today status end -->




  </section>
  <!-- container section start -->
  <?php
  include '../footer.php'; ?>
