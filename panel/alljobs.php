  <?php
ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '100M');
ini_set('max_input_time', 300);
ini_set('max_execution_time', 300);

  include '../header.php'; ?>

    <body>
    <!-- container section start -->
    <section id="container" class="">


      <?php include '../topbar.php'; ?>
        <!--header end-->

        <!--sidebar start-->
      <?php include '../leftbar.php';
      ?>
        <!--sidebar end-->

        <!--main content start-->
        <section id="main-content" style="overflow-y: scroll;">
          <section class="wrapper">
      <div class="row">
        <div class="col-lg-12">
            <h3 class="username"> Welcome <?php  print_r($_SESSION['logged_in']['username']); ?></h3>
          <h3 class="page-header"><i class="fa fa-table"></i> Report</h3>
          <ol class="breadcrumb">
            <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
            <li><i class="fa fa-table"></i>Report</li>
            <li><i class="fa fa-th-list"></i>Executive Report</li>
          </ol>
        </div>
      </div>
              <!-- page start-->


  <form method="post">
  <div class="row">
  <div class="form-group ">



            <?php

            if ($_SESSION['logged_in']['usertype']==1)
            {
            $employe = "SELECT `id`, `cityname` FROM `city` WHERE delid =0";
            $equery = mysqli_query($conn,$employe);
  echo '
  <div class="col-md-4">
      <label for="etype" class="control-label col-md-1"> City </label>
    <select class="form-control m-bot15" id="city" name="city" >
      <option value="">--Please Select Employee City</option>';
            while ($edata = mysqli_fetch_assoc($equery)) {
            echo '  <option value='.$edata['id'].'>'.$edata['cityname'].'</option>';
  }
  echo'</select>

          </div> ';


        }
          else {
            echo '<div class="col-md-4">

            <input  id="city" name="city" hidden >
            </div>';
          }
           ?>



        <?php

        if ($_SESSION['logged_in']['usertype']==1)
        {
          echo '  <div class="col-md-4">
              <label for="etype" class="control-label col-md-1">Manager </label>
              <select class="form-control m-bot15" id="etype" name="etype" >
                <option value="">--Please Select Manager Name</option>
            </select>
            </div>';
        }
        else {
          echo '<input  id="etype" name="etype" hidden>';
        }
        ?>

        <?php

        if ($_SESSION['logged_in']['usertype']==1)
        {
          echo '  <div class="col-md-4">
              <label for="etype" class="control-label col-md-1">Executive </label>
              <select class="form-control m-bot15" id="executivename" name="executivename" >
                <option value="">--Please Select Executive Name</option>


            </select>
            </div>
        </div>';
        }
        else {
            $loggedin = $_SESSION['logged_in']['id'];
            echo ' <div class="col-md-4">
                <label for="etype" class="control-label col-md-1">Executive </label>
                <select class="form-control m-bot15" id="executivename" name="executivename" >
                    <option value="">--Please Select Executive Name</option>';
                    $employe = "SELECT `id`, `fullname` FROM `executive` WHERE delid =0 AND managerid =$loggedin ";
                    $equery = mysqli_query($conn,$employe);
                    while ($edata = mysqli_fetch_assoc($equery)) {
                    echo "  <option value=".$edata['id'].">".$edata['fullname']."</option>
                    ";
                    }



            echo '  </select>
              </div>';


        }

        ?>



  </div>
  </div>
  <div class="row">
    <div class="form-group ">

        <div class="col-md-6">
            <label for="etype" class="control-label col-md-1"> Start Date </label>
    <input type="date" class="form-control" id="sdate" name="sdate" placeholder="Enter City Name">
        </div>
        <div class="col-md-6">
          <label for="etype" class="control-label col-md-1">End Date </label>
    <input type="date" class="form-control" id="edate" name="edate" placeholder="Enter City Name">
        </div>

    </div>

  </div>

  <div class="col-md-4">
      <button class="btn btn-primary" id="search" name="search"  style="margin-top: 10px;">Search</button>
  </div>


  </form>
  <!-- <div class="col-md-4">
      <button class="btn btn-primary"   style="margin-top: 10px;" onclick="scrollWin1()"  >Left Scroll</button>
  </div>
  <div class="col-md-4">
      <button class="btn btn-primary" style="margin-top: 10px;" onclick="scrollWin()" >Right Scroll</button>
  </div> -->
  <?php
  if(isset($_POST['search']))
  {
    $jobs = array();
    $city = $_POST['city'];
    $manager = $_POST['etype'];
    $executive = $_POST['executivename'];
    $sdate = $_POST['sdate'];
    $edate = $_POST['edate'];
    //
      // echo 'city'.$city;
      // echo '<br>';
      // echo 'Manager'.$manager;
      // echo '<br>';
      // echo 'executive'.$executive;
      // echo '<br>';
      // echo 'sdate'.$sdate;
      // echo '<br>';
      // echo 'edate'.$edate;



  if($_SESSION['logged_in']['usertype']==1)
  {
  //  if($city!='' && $manager =='' && $executive =='' && $edate =='' && $sdate ==' ')
    if($city!='' && $manager =='' && $executive =='' )

    {
      if($edate=='' && $sdate=='')
      {

      $userlist = "SELECT city.cityname as cityname ,newjob.remarks as remarks,newjob.savedon as savedon,newjob.claimno as claimno,newjob.status as status,newjob.mobile as mobile,newjob.description as description,executive.fullname as fullname,newjob.rsavedon as rsavedon FROM newjob JOIN executive,jobdetails,city WHERE executive.id = newjob.employeid  and newjob.branchid = city.id AND  newjob.id = jobdetails.jobid and newjob.branchid ='$city'  ORDER BY newjob.status ASC";


        $userquery = mysqli_query($conn,$userlist);

        while ($data = mysqli_fetch_assoc($userquery)) {
          $jobs[] = $data;
        }
        // print_r($jobs);
        //echo '1.1';
        // echo $userlist;
      }
      else {
        $edate = date("d/m/Y", strtotime($edate));
        $sdate = date("d/m/Y", strtotime($sdate));
        $userlist = " SELECT city.cityname as cityname ,newjob.remarks as remarks,newjob.savedon as savedon,newjob.claimno as claimno,newjob.status as status,newjob.mobile as mobile,newjob.description as description,executive.fullname as fullname,newjob.rsavedon as rsavedon FROM newjob JOIN executive,jobdetails,city WHERE executive.id = newjob.employeid  and newjob.branchid = city.id AND newjob.branchid ='$city'  and  newjob.date BETWEEN '$sdate' AND '$edate' ORDER BY newjob.status ASC";

        $userquery = mysqli_query($conn,$userlist);

        while ($data = mysqli_fetch_assoc($userquery)) {
          $jobs[] = $data;
        }
        // print_r($userlist);
      //echo '1.2';
        // echo $userlist;
      }


    }
  else if($manager !='' &&$executive=='' )

    {
      if($edate=='' && $sdate=='')
      {

        $userlist = "SELECT city.cityname as cityname ,newjob.remarks as remarks,newjob.savedon as savedon,newjob.claimno as claimno,newjob.status as status,newjob.mobile as mobile,newjob.description as description,executive.fullname as fullname,newjob.rsavedon as rsavedon FROM newjob JOIN executive,jobdetails,city WHERE executive.id = newjob.employeid  and newjob.branchid = city.id AND newjob.branchid ='$city'  and  newjob.managerid = '$manager' ORDER BY newjob.status ASC";


        $userquery = mysqli_query($conn,$userlist);

        while ($data = mysqli_fetch_assoc($userquery)) {
          $jobs[] = $data;
        }
         //print_r($jobs);
          //echo '2.1';
        //   echo $userlist;
      }
      else {
        # code...
        $edate = date("d/m/Y", strtotime($edate));
        $sdate = date("d/m/Y", strtotime($sdate));
        $userlist = "SELECT city.cityname as cityname ,newjob.remarks as remarks,newjob.savedon as savedon,newjob.claimno as claimno,newjob.status as status,newjob.mobile as mobile,newjob.description as description,executive.fullname as fullname,newjob.rsavedon as rsavedon FROM newjob JOIN executive,jobdetails,city WHERE executive.id = newjob.employeid  and newjob.branchid = city.id AND  newjob.branchid ='$city'  and  newjob.managerid = '$manager' AND  date between  '$sdate' and '$edate'  ORDER BY newjob.status ASC";


        $userquery = mysqli_query($conn,$userlist);

        while ($data = mysqli_fetch_assoc($userquery)) {
          $jobs[] = $data;
        }
         //print_r($jobs);
        //  echo '2.2';
        //   echo $userlist;
      }


    }
      else if ($executive != '' ) {


        if($edate=='' && $sdate=='')
        {
          $userlist = "SELECT city.cityname as cityname , newjob.remarks as remarks,newjob.savedon as savedon,newjob.claimno as claimno,newjob.status as status,newjob.mobile as mobile,newjob.description as description,executive.fullname as fullname,newjob.rsavedon as rsavedon FROM newjob JOIN executive,jobdetails,city WHERE executive.id = newjob.employeid  and newjob.branchid = city.id and  newjob.employeid ='$executive'   ORDER BY newjob.status ASC";


      $userquery = mysqli_query($conn,$userlist);

      while ($data = mysqli_fetch_assoc($userquery)) {
        $jobs[] = $data;
      }
    //  echo $userlist;
      //  echo '3.1';
     }
     else {
       $edate = date("d/m/Y", strtotime($edate));
       $sdate = date("d/m/Y", strtotime($sdate));
       $userlist = "SELECT city.cityname as cityname ,newjob.remarks as remarks,newjob.savedon as savedon,newjob.claimno as claimno,newjob.status as status,newjob.mobile as mobile,newjob.description as description,executive.fullname as fullname,newjob.rsavedon as rsavedon FROM newjob JOIN executive,jobdetails,city WHERE executive.id = newjob.employeid  and newjob.branchid = city.id AND newjob.employeid ='$executive'   and   date between  '$sdate' and '$edate'  ORDER BY newjob.status ASC";
       $userquery = mysqli_query($conn,$userlist);

       while ($data = mysqli_fetch_assoc($userquery)) {
         $jobs[] = $data;
       }
        // echo '3.2';
     }
      // // print_r($jobs);
      //  echo $userlist;
    }

    else {

      $edate = date("d/m/Y", strtotime($edate));
      $sdate = date("d/m/Y", strtotime($sdate));
      $userlist = "SELECT city.cityname as cityname ,newjob.remarks as remarks,newjob.readid  as readid,newjob.remarks as remarks, jobdetails.savedtime as ctime,newjob.id as jobid,newjob.claimno as claimno ,newjob.description as description,newjob.mobile as mobile,newjob.savedon as jobsavetime,jobdetails.modelno as modelno,jobdetails.serialno as serialno,jobdetails.distance as distance,jobdetails.cfeedback as cfeedback,jobdetails.warrenty as warrenty,jobdetails.status as status,jobdetails.image1 as image1,jobdetails.image2 as image2 ,jobdetails.image3 as image3,jobdetails.image4 as image4,jobdetails.image5 as image5 ,jobdetails.image6 as image6,usermaster.username as username,usermaster.fullname as ufullname,usermaster.fullname as managername,newjob.savedby  as savedby,executive.fullname as efullname,jobdetails.location as location FROM newjob JOIN executive , jobdetails,usermaster WHERE newjob.id = jobdetails.jobid AND  usermaster.id = newjob.managerid and newjob.status !=0 AND executive.id = newjob.employeid  and date between  '$sdate' and '$edate' ORDER BY jobdetails.savedtime DESC";
 echo '5';
//       echo $userlist;

      $cityquery = mysqli_query($conn,$userlist);

    $i=1;
    while ($data = mysqli_fetch_assoc($cityquery)) {
      $jobs[] = $data;
    }
 //print_r($jobs);
    }

  }
  else {
    if( $executive !='' )

    {
      if($edate=='' && $sdate=='')
      {
        $loggedin = $_SESSION['logged_in']['id'];
        $userlist = "SELECT city.cityname as cityname ,newjob.remarks as remarks,newjob.readid  as readid,newjob.savedby  as savedby,newjob.remarks as remarks, jobdetails.savedtime as ctime,newjob.id as jobid,newjob.claimno as claimno ,newjob.description as description,newjob.mobile as mobile,newjob.savedon as jobsavetime,jobdetails.modelno as modelno,jobdetails.serialno as serialno,jobdetails.distance as distance,jobdetails.cfeedback as cfeedback,jobdetails.warrenty as warrenty,jobdetails.status as status,jobdetails.image1 as image1,jobdetails.image2 as image2 ,jobdetails.image3 as image3,jobdetails.image4 as image4,jobdetails.image5 as image5 ,jobdetails.image6 as image6,usermaster.username as username,usermaster.fullname as ufullname,usermaster.fullname as managername,executive.fullname as efullname,jobdetails.location as location  FROM newjob JOIN executive ,jobdetails,usermaster WHERE newjob.id = jobdetails.jobid AND  usermaster.id = newjob.managerid  and newjob.status !=0 AND executive.id = newjob.employeid AND newjob.employeid = $executive ORDER BY jobdetails.savedtime DESC";

        $userquery = mysqli_query($conn,$userlist);

        while ($data = mysqli_fetch_assoc($userquery)) {
          $jobs[] = $data;
        }
      }
      else {
        # code...
        $edate = date("d/m/Y", strtotime($edate));
        $sdate = date("d/m/Y", strtotime($sdate));

        $loggedin = $_SESSION['logged_in']['id'];
        $userlist = "SELECT city.cityname as cityname ,newjob.remarks as remarks,newjob.readid  as readid,newjob.savedby  as savedby,newjob.remarks as remarks, jobdetails.savedtime as ctime,newjob.id as jobid,newjob.claimno as claimno ,newjob.description as description,newjob.mobile as mobile,newjob.savedon as jobsavetime,jobdetails.modelno as modelno,jobdetails.serialno as serialno,jobdetails.distance as distance,jobdetails.cfeedback as cfeedback,jobdetails.warrenty as warrenty,jobdetails.status as status,jobdetails.image1 as image1,jobdetails.image2 as image2 ,jobdetails.image3 as image3,jobdetails.image4 as image4,jobdetails.image5 as image5 ,jobdetails.image6 as image6,usermaster.username as username,usermaster.fullname as ufullname,usermaster.fullname as managername,executive.fullname as efullname,jobdetails.location as location  FROM newjob JOIN executive ,jobdetails,usermaster WHERE newjob.id = jobdetails.jobid AND  usermaster.id = newjob.managerid  and newjob.status !=0 AND executive.id = newjob.employeid AND newjob.managerid = $loggedin AND date between  '$sdate' and '$edate' ORDER BY jobdetails.savedtime DESC";

        $userquery = mysqli_query($conn,$userlist);

        while ($data = mysqli_fetch_assoc($userquery)) {
          $jobs[] = $data;
        }
      }


      }


    }
  }

  //after click search button
  else {
    if ($_SESSION['logged_in']['usertype']==1)
    {
      $edate = date("d/m/Y");
      $eedate = date('d/m/Y');
      $sedate = str_replace('/','-',$edate);
      $sdate = date('d/m/Y', strtotime($sedate.'-2 day'));
      //$sdate = date("d/m/Y", strtotime($sdate));
      $userlist = "SELECT city.cityname as cityname ,newjob.remarks as remarks,newjob.savedon as savedon,newjob.claimno as claimno,newjob.status as status,newjob.mobile as mobile,newjob.description as description,executive.fullname as fullname,newjob.rsavedon as rsavedon FROM newjob JOIN executive,city WHERE executive.id = newjob.employeid  and newjob.branchid = city.id and  newjob.date BETWEEN '$sdate' AND '$edate' ORDER BY newjob.status ASC";

      $userquery = mysqli_query($conn,$userlist);
      while ($data = mysqli_fetch_assoc($userquery)) {
        $jobs[] = $data;
      }
      echo $userlist;
  }
  else {
  $edate = date("d/m/Y");
  $eedate = date('d/m/Y');
  $sedate = str_replace('/','-',$edate);
  $sdate = date('d/m/Y', strtotime($sedate.'-2 day'));
    $loggedin = $_SESSION['logged_in']['id'];
    $userlist = "SELECT city.cityname as cityname ,newjob.remarks as remarks,newjob.savedon as savedon,newjob.claimno as claimno,newjob.status as status,newjob.mobile as mobile,newjob.description as description,executive.fullname as fullname,newjob.rsavedon as rsavedon FROM newjob JOIN executive,city WHERE executive.id = newjob.employeid  and newjob.branchid = city.id AND newjob.managerid = '$loggedin' and newjob.date BETWEEN '$sdate' AND '$edate' ORDER BY newjob.status ASC";

    $userquery = mysqli_query($conn,$userlist);
    $i = 1;
    while ($data = mysqli_fetch_assoc($userquery)) {
      $jobs[] = $data;
    }

  }
}


 ?>
            <div class="row">
                <div class="col-lg-12">
                  <div class="btn-group">
                              <button onClick ="$('#example').tableExport({type:'excel',escape:'false'});" class="btn btn-warning btn-sm dropdown-toggle" style="float:center;margin-top: 20px;" type="submit" name="btn-download" id="btndownload" data-toggle="dropdown"><i class="fa fa-bars"></i>Download</button>
                                <!-- <ul class="dropdown-menu " role="menu">



                  <li><a href="#" onClick ="$('#example').tableExport({type:'excel',escape:'false'});"> <img src='../assets/reportjs/xls.png' width='24px'> XLS</a></li>



                  </ul> -->
                              </div>
                    <section class="panel" id="newtable">


                        <header class="panel-heading">
                            Advanced Table
                        </header>

                        <table id="example" class="table">
  			<thead>
        <tr>
            <th style="  text-align: center;">S.No</th>
            <th style="  text-align: center;">Assign Date</th>
               <th style="  text-align: center;">Executive Name</th>
               <th style="  text-align: center;">City Name</th>
          <th style="  text-align: center;">Claim No.</th>

                <th style="  text-align: center;">Job Status</th>
                 <th style="  text-align: center;">Remarks</th>
                  <th style="  text-align: center;">Complete Time</th>
                   <th style="  text-align: center;">Phone Number</th>

                     <th style="  text-align: center;">Description</th>




          <!-- <th>Description</th>
          <th>Mobile No.</th> -->



        </tr>
      </thead>
      <tbody id="tBody">
        <?php

        if(count($jobs)>0)
        {


          $i = 1;
          for($j=0;$j<count($jobs);$j++) {

          // print_r($jobs);

          echo '<tr>
            <td style="  text-align: center;">'.$i.'</td>

            <td style="  text-align: center;">'.$jobs[$j]['savedon'].'</td>
  <td style="  text-align: center;">'.$jobs[$j]['fullname'].'</td>
  <td style="  text-align: center;">'.$jobs[$j]['cityname'].'</td>
            <td style="  text-align: center;">'.$jobs[$j]['claimno'].'</td>

              ';
              if($jobs[$j]['status']==2)
              {
                echo '<td style="  text-align: center;">Repair Complete</td>';
              }
              elseif ($jobs[$j]['status']==3) {
                echo '<td style="  text-align: center;">Part Pending</td>';
              }
              elseif ($jobs[$j]['status']==4) {
                echo '<td style="  text-align: center;">Pending</td>';
              }
              elseif ($jobs[$j]['status']==5) {
                echo '<td style="  text-align: center;">Cancel</td>';
              }

              else if($jobs[$j]['status']==6){
                echo '<td style="  text-align: center;">Decline</td>';
              }
              elseif ($jobs[$j]['status']==0) {
                # code...
                echo '<td style="  text-align: center;">Not Submitted</td>';
              }
              else {
                echo '<td style="  text-align: center;">Select</td>';
              }
              echo '<td style="  text-align: center;">'.$jobs[$j]['remarks'].'</td>
              <td style="  text-align: center;">'.$jobs[$j]['rsavedon'].'</td>
              <td style="  text-align: center;">'.$jobs[$j]['mobile'].'</td>
              <td style="  text-align: center;">'.$jobs[$j]['description'].'</td>';




        echo '

          </tr>';
          $i++;
          }
}

         ?>


      </tbody>
    </table>
  		                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
          <div class="text-right">
          <div class="credits">
                <!--
                    All the links in the footer should remain intact.
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
                -->
                <a href="#"> </a> Powered by <a href="http://www.illywhackertechnologies.com/">Illywhackertechnologies.com</a>
            </div>
        </div>
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->
  <?php
  include '../footer/report_footer.php'; ?>
