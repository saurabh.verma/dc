<?php
ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '100M');
ini_set('max_input_time', 300);
ini_set('max_execution_time', 300);

include '../header.php'; ?>

  <body>
  <!-- container section start -->
  <section id="container" class="">


    <?php include '../topbar.php'; ?>
      <!--header end-->

      <!--sidebar start-->
    <?php include '../leftbar.php';
    ?>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content" style="overflow-y: scroll;">
        <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <h3 class="username"> Welcome <?php  //($_SESSION['logged_in']['username']); ?></h3>
        <h3 class="page-header"><i class="fa fa-table"></i> Report</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
          <li><i class="fa fa-table"></i>Report</li>
          <li><i class="fa fa-th-list"></i>Daily Report</li>
        </ol>
      </div>
    </div>
            <!-- page start-->

<form method="post">
            <div class="row">
              <div class="form-group ">

                  <div class="col-md-4">
                      <label for="etype" class="control-label col-md-1"> City </label>
                    <select class="form-control m-bot15" id="city" name="city" >
                      <option value="">--Please Select Employee City</option>
                      <?php

                      if ($_SESSION['logged_in']['usertype']==1)
                      {
                      $employe = "SELECT `id`, `cityname` FROM `city` WHERE delid =0";
                      $equery = mysqli_query($conn,$employe);

                      while ($edata = mysqli_fetch_assoc($equery)) {
                      echo "  <option value=".$edata['id'].">".$edata['cityname']."</option> ";
                      }
                    }
                    else {
                    $city =    $_SESSION['logged_in']['usercity'];
                      $employe = "SELECT `id`, `cityname` FROM `city` WHERE delid =0 and id =$city ";
                      $equery = mysqli_query($conn,$employe);

                      while ($edata = mysqli_fetch_assoc($equery)) {
                      echo "  <option  value=".$edata['id'].">".$edata['cityname']."</option> ";
                      }
                    } ?>

                  </select>
                  </div>
                  <div class="col-md-4">
                    <label for="etype" class="control-label col-md-1">Manager </label>
                    <select class="form-control m-bot15" id="etype" name="etype" >
                      <option value="">--Please Select Manager Name</option>


                  </select>
                  </div>
                  <div class="col-md-4">
                    <label for="etype" class="control-label col-md-1">Executive </label>
                    <select class="form-control m-bot15" id="executivename" name="executivename" >
                      <option value="">--Please Select Executive Name</option>


                  </select>
                  </div>
              </div>

            </div>
            <div class="row">
              <input id="logged" hidden value="<?php  echo  $_SESSION['logged_in']['id'];?>">
              <div class="form-group ">

                  <div class="col-md-6">
                      <label for="etype" class="control-label col-md-1"> Start Date </label>
              <input type="date" class="form-control" id="sdate" name="sdate" placeholder="Enter City Name">
                  </div>
                  <div class="col-md-6">
                    <label for="etype" class="control-label col-md-1">End Date </label>
              <input type="date" class="form-control" id="edate" name="edate" placeholder="Enter City Name">
                  </div>

              </div>

            </div>
            <div class="col-md-4">
                <button class="btn btn-primary" id="search" name="search"  style="margin-top: 10px;">Search</button>
            </div>

            </form>
            <?php
            if(isset($_POST['search']))
            {
              $jobs = array();
              $city = $_POST['city'];
              $manager = $_POST['etype'];
              $executive = $_POST['executivename'];
              $sdate = $_POST['sdate'];
              $edate = $_POST['edate'];

              if($_SESSION['logged_in']['usertype']==1)
              {
                //show data  of managers
                if($manager !='' && $executive=='')
                {
                  if($sdate =='' && $edate =='')
                  {
                    echo "<script>alert('please Select A date')</script>";
                  }

                  else {
                    # code...

                    $userlist = "SELECT `id` FROM `executive` WHERE delid = 0 AND `managerid`='$manager'";
                    $userquery = mysqli_query($conn,$userlist);
                    //$i = 1;
                    while ($data = mysqli_fetch_assoc($userquery)) {
                      $mangr[] = $data['id'];
                    }
                    //manager
                    for($i=0;$i<count($mangr);$i++)
                    {
                      $id = $mangr[$i];
                      $date = date("d/m/Y", strtotime($sdate));
                      $sdate = date("d/m/Y", strtotime($sdate));
                      $edate = date("d/m/Y", strtotime($edate));
                      // while ($sdate<=$edate) {
                        # code...


                      $totaljob = "SELECT city.cityname as cityname,COUNT(newjob.id) as idcount,newjob.date as date,executive.fullname as fullname FROM newjob JOIN executive,city  WHERE   newjob.employeid = executive.id AND newjob.employeid = $id   AND newjob.date = '$date' AND city.id = executive.usercity ORDER BY newjob.date DESC";
                      $totaljobquery = mysqli_query($conn,$totaljob);
                      $totaldata = mysqli_fetch_assoc($totaljobquery);
                        $totaljobarray[] =  $totaldata;



                        $complete = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 2  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                        $completequery = mysqli_query($conn,$complete);
                      $completedata = mysqli_fetch_assoc($completequery);
                          $completejobarray[] =  $completedata;

                          $partpending = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE   newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 3   AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                          $partpendingquery = mysqli_query($conn,$partpending);
                      $partpendingdata = mysqli_fetch_assoc($partpendingquery);
                            $partpendingjobarray[] =  $partpendingdata;

                            $pending = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 4  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                            $pendingquery = mysqli_query($conn,$pending);
                      $pendingdata = mysqli_fetch_assoc($pendingquery);
                              $pendingjobarray[] =  $pendingdata;

                              $cancel = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 5   AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                          $cancelquery = mysqli_query($conn,$cancel);
                      $canceldata = mysqli_fetch_assoc($cancelquery);
                                $canceljobarray[] =  $canceldata;

                                $decline = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 6  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                            $declinequery = mysqli_query($conn,$decline);
                        $declinedata = mysqli_fetch_assoc($declinequery);
                                $declinejobarray[] =  $declinedata;

                                $selet = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 1  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                            $seletquery = mysqli_query($conn,$selet);
                        $seletdata = mysqli_fetch_assoc($seletquery);
                                $seletjobarray[] =  $seletdata;

                    }
                    // $sdate = date('d/m/Y', strtotime($sdate . ' +1 day'));
                    // }
                  }


                }
                elseif ($executive !='') {
                  # code...
                if($sdate =='' && $edate =='')
                {
                  $id = $executive;
                  //    echo $maangerid;
                  $userlist = "SELECT DISTINCT(newjob.date) as date FROM newjob JOIN executive WHERE newjob.employeid = executive.id AND newjob.employeid = $id ORDER BY newjob.date ASC";

                  $userquery = mysqli_query($conn,$userlist);
                  //$i = 1;
                  while ($data = mysqli_fetch_assoc($userquery)) {

                  $date = $data['date'];

                    $totaljob = "SELECT  city.cityname as cityname,COUNT(newjob.id) as idcount,newjob.date as date,executive.fullname as fullname FROM newjob JOIN executive,city  WHERE   newjob.branchid = city.id AND newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.date = '$date'     ORDER BY newjob.date DESC";
                    $totaljobquery = mysqli_query($conn,$totaljob);
                    $totaldata = mysqli_fetch_assoc($totaljobquery);
                      $totaljobarray[] =  $totaldata;


                      $complete = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 2 AND newjob.date = '$date'    ORDER BY newjob.date DESC";

                      $completequery = mysqli_query($conn,$complete);
                    $completedata = mysqli_fetch_assoc($completequery);
                        $completejobarray[] =  $completedata;

                        $partpending = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE   newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 3 AND newjob.date = '$date'     ORDER BY newjob.date DESC";

                        $partpendingquery = mysqli_query($conn,$partpending);
                    $partpendingdata = mysqli_fetch_assoc($partpendingquery);
                          $partpendingjobarray[] =  $partpendingdata;

                          $pending = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 4 AND newjob.date = '$date'     ORDER BY newjob.date DESC";

                          $pendingquery = mysqli_query($conn,$pending);
                    $pendingdata = mysqli_fetch_assoc($pendingquery);
                            $pendingjobarray[] =  $pendingdata;

                            $cancel = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 5  AND newjob.date = '$date'     ORDER BY newjob.date DESC";

                        $cancelquery = mysqli_query($conn,$cancel);
                    $canceldata = mysqli_fetch_assoc($cancelquery);
                              $canceljobarray[] =  $canceldata;

                              $decline = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 6  AND newjob.date = '$date'    ORDER BY newjob.date DESC";

                          $declinequery = mysqli_query($conn,$decline);
                      $declinedata = mysqli_fetch_assoc($declinequery);
                              $declinejobarray[] =  $declinedata;

                              $seletquery = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 1  AND newjob.date = '$date'    ORDER BY newjob.date DESC";

                          $seletquery = mysqli_query($conn,$seletquery);
                      $seletdata = mysqli_fetch_assoc($seletquery);
                              $seletjobarray[] =  $seletdata;

                  }
                }
                //if date are given
                else {
                  # code...
                  $edate = date("d/m/Y", strtotime($edate));
                  $sdate = date("d/m/Y", strtotime($sdate));

                  echo $sdate;
                  echo "<br>";
                  echo $edate;
                  while ($sdate<=$edate) {

                    echo $sdate;
                    echo "<br>";

                  $sdate = date('d/m/Y', strtotime('+1 day',$sdate));
                  }

                }



                }

              }
            }


              //before click search button
              else {
                if ($_SESSION['logged_in']['usertype']==1)
                {
                  $userlist = "SELECT `id` FROM `executive` WHERE delid = 0";

                  $userquery = mysqli_query($conn,$userlist);
                  //$i = 1;
                  while ($data = mysqli_fetch_assoc($userquery)) {
                  $date = date('d/m/Y');
                  //  //($data);
                    $id = $data['id'];
                    $totaljob = "SELECT city.cityname as cityname,COUNT(newjob.id) as idcount,newjob.date as date,executive.fullname as fullname FROM newjob JOIN executive,city  WHERE   newjob.employeid = executive.id AND newjob.employeid = $id   AND newjob.date = '$date' AND city.id = executive.usercity ORDER BY newjob.date DESC";
                    $totaljobquery = mysqli_query($conn,$totaljob);
                    $totaldata = mysqli_fetch_assoc($totaljobquery);
                      $totaljobarray[] =  $totaldata;

                      $complete = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 2  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                      $completequery = mysqli_query($conn,$complete);
                    $completedata = mysqli_fetch_assoc($completequery);
                        $completejobarray[] =  $completedata;

                        $partpending = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE   newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 3   AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                        $partpendingquery = mysqli_query($conn,$partpending);
                    $partpendingdata = mysqli_fetch_assoc($partpendingquery);
                          $partpendingjobarray[] =  $partpendingdata;

                          $pending = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 4  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                          $pendingquery = mysqli_query($conn,$pending);
                    $pendingdata = mysqli_fetch_assoc($pendingquery);
                            $pendingjobarray[] =  $pendingdata;

                            $cancel = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 5   AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                        $cancelquery = mysqli_query($conn,$cancel);
                    $canceldata = mysqli_fetch_assoc($cancelquery);
                              $canceljobarray[] =  $canceldata;

                              $decline = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 6  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                          $declinequery = mysqli_query($conn,$decline);
                      $declinedata = mysqli_fetch_assoc($declinequery);
                              $declinejobarray[] =  $declinedata;

                              $selet = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 1  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                          $seletquery = mysqli_query($conn,$selet);
                      $seletdata = mysqli_fetch_assoc($seletquery);
                              $seletjobarray[] =  $seletdata;
                  }
              }
              else {
                $maangerid = $_SESSION['logged_in']['id'];
              //    echo $maangerid;
              $userlist = "SELECT `id` FROM `executive` WHERE delid = 0 AND `managerid`='$maangerid'";

              $userquery = mysqli_query($conn,$userlist);
              //$i = 1;
              while ($data = mysqli_fetch_assoc($userquery)) {
              $date = date('d/m/Y');
              //  //($data);
                $id = $data['id'];
                $totaljob = "SELECT COUNT(newjob.id) as idcount,newjob.date as date,executive.fullname as fullname FROM newjob JOIN executive  WHERE   newjob.employeid = executive.id AND newjob.employeid = $id   AND newjob.date = '$date'  ORDER BY newjob.date DESC";
                $totaljobquery = mysqli_query($conn,$totaljob);
                $totaldata = mysqli_fetch_assoc($totaljobquery);
                  $totaljobarray[] =  $totaldata;

                  $complete = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 1  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                  $completequery = mysqli_query($conn,$complete);
                $completedata = mysqli_fetch_assoc($completequery);
                    $completejobarray[] =  $completedata;

                    $partpending = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE   newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 2   AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                    $partpendingquery = mysqli_query($conn,$partpending);
                $partpendingdata = mysqli_fetch_assoc($partpendingquery);
                      $partpendingjobarray[] =  $partpendingdata;

                      $pending = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 3  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                      $pendingquery = mysqli_query($conn,$pending);
                $pendingdata = mysqli_fetch_assoc($pendingquery);
                        $pendingjobarray[] =  $pendingdata;

                        $cancel = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 4   AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                    $cancelquery = mysqli_query($conn,$cancel);
                $canceldata = mysqli_fetch_assoc($cancelquery);
                          $canceljobarray[] =  $canceldata;

                          $decline = "SELECT COUNT(newjob.id) as idcount FROM newjob JOIN executive  WHERE  newjob.employeid = executive.id AND newjob.employeid = $id AND newjob.status = 5  AND newjob.date = '$date'  ORDER BY newjob.date DESC";

                      $declinequery = mysqli_query($conn,$decline);
                  $declinedata = mysqli_fetch_assoc($declinequery);
                          $declinejobarray[] =  $declinedata;
              }

              }
            }


             ?>

<br>

            <div class="row">
                <div class="col-lg-12">
                  <div class="btn-group">
                              <button class="btn btn-warning btn-sm dropdown-toggle" style="float:center;margin-top: 20px;" type="submit" name="btn-download" id="btndownload" data-toggle="dropdown"><i class="fa fa-bars"></i>Download</button>
                                <ul class="dropdown-menu " role="menu">



                  <li><a href="#" onClick ="$('#example').tableExport({type:'excel',escape:'false'});"> <img src='../assets/reportjs/xls.png' width='24px'> XLS</a></li>



                  </ul>
                              </div>

                    <section class="panel" id="newtable">
                      <!-- <button id="import" >import</button> -->


                        <header class="panel-heading">
                            Advanced Table
                        </header>

                        <table id="example" class="table">
  			<thead>
        <tr>
            <th>S.No</th>
          <th>Date</th>
          <th>Executive Name</th>
            <th>Executive City</th>
            <th> Assigned  Jobs</th>
              <th> Completed Jobs</th>
              <th>Part Pending Jobs</th>
              <th> Pending Jobs</th>
              <th> Cancel Jobs</th>
                <th> Declined Jobs</th>
                  <th> Undefined Jobs</th>


        </tr>
      </thead>
      <tbody id="tBody">
        <?php
        if ($_SESSION['logged_in']['usertype']==1)
        {


        $i=1;
        for($j=0;$j<count($totaljobarray);$j++)
        {
        //  //($pendingjobarray[$j]);
          if($totaljobarray[$j]['date']=='')
          {

          }
          else {
            echo '<tr>
              <td style="  text-align: center;">'.$i.'</td>
              <td style="  text-align: center;">'.$totaljobarray[$j]['date'].'</td>
              <td style="  text-align: center;">'.$totaljobarray[$j]['fullname'].'</td>
                <td style="  text-align: center;">'.$totaljobarray[$j]['cityname'].'</td>
                <td style="  text-align: center;">'.$totaljobarray[$j]['idcount'].'</td>
                  <td style="  text-align: center;">'.$completejobarray[$j]['idcount'].'</td>
                    <td style="  text-align: center;">'.$partpendingjobarray[$j]['idcount'].'</td>
                      <td style="  text-align: center;">'.$pendingjobarray[$j]['idcount'].'</td>
                        <td style="  text-align: center;">'.$canceljobarray[$j]['idcount'].'</td>
                        <td style="  text-align: center;">'.$declinejobarray[$j]['idcount'].'</td>
                          <td style="  text-align: center;">'.$seletjobarray[$j]['idcount'].'</td>


            </tr>';
            $i++;
          }


        }
      }
      else {



        $i=1;
        for($j=0;$j<count($totaljobarray);$j++)
        {
        //  //($pendingjobarray[$j]);
          if($totaljobarray[$j]['date']=='')
          {

          }
          else {
            echo '<tr>
              <td>'.$i.'</td>
              <td>'.$totaljobarray[$j]['date'].'</td>
              <td>'.$totaljobarray[$j]['fullname'].'</td>
                <td>'.$totaljobarray[$j]['idcount'].'</td>
                  <td>'.$completejobarray[$j]['idcount'].'</td>
                    <td>'.$partpendingjobarray[$j]['idcount'].'</td>
                      <td>'.$pendingjobarray[$j]['idcount'].'</td>
                        <td>'.$canceljobarray[$j]['idcount'].'</td>
                        <td>'.$declinejobarray[$j]['idcount'].'</td>


            </tr>';
            $i++;
          }


        }
      } ?>


      </tbody>
    </table>
  		                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
          <div class="text-right">
          <div class="credits">
                <!--
                    All the links in the footer should remain intact.
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
                -->
                <a href="#"> </a> Powered by <a href="http://www.illywhackertechnologies.com/">Illywhackertechnologies.com</a>
            </div>
        </div>
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->
  <?php
  include '../footer/dreport_footer.php'; ?>
