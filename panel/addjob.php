<?php

ob_start();
include '../header.php'; ?>

  <body>
  <!-- container section start -->
  <section id="container" class="">


    <?php include '../topbar.php'; ?>
      <!--header end-->

      <!--sidebar start-->
    <?php include '../leftbar.php';
    ?>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <h3 class="username"> Welcome <?php  print_r($_SESSION['logged_in']['username']); ?></h3>
        <h3 class="page-header"><i class="fa fa-files-o"></i> Add New Job</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="../panel/dash.php">Home</a></li>
          <li><i class="icon_document_alt"></i>Job</li>
          <li><i class="fa fa-files-o"></i>New Jobs</li>
        </ol>
      </div>
    </div>
            <!-- Form validations -->
            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Form Tittle</h4>
                                    </div>
                                    <div class="modal-body">

                                        <!-- <form role="form" method="post"> -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">City Name</label>
                                                <input type="email" class="form-control" id="cityname" name="cityname" placeholder="Enter City Name">
                                            </div>


                                            <button type="submit" class="btn btn-primary" id="csave" name="csave">Save</button>
                                        <!-- </form> -->
                                    </div>
                                </div>
                            </div>
                        </div>

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                           Add New Job
                        </header>


                        <div class="panel-body">
                            <div class="form">

                                <form class="form-validate form-horizontal " id="register_form" method="post">
                                    <input id="userid" name="userid" hidden   value="<?php  print_r($_SESSION['logged_in']['id']); ?>">
                                    <div class="form-group ">
                                        <label for="fullname" class="control-label col-lg-2">Claim  No: <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="claimno" name="claimno" type="text"  required >
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="mobile" class="control-label col-lg-2">Mobile No<span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="mobileno" name="mobileno" type="text"   maxlength="10" value="" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="email" class="control-label col-lg-2">Description <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <textarea class="form-control " id="description" name="description" type="text"  ></textarea>
                                        </div>
                                    </div>



                                    <div class="form-group ">
                                        <label for="city" class="control-label col-lg-2">Employee City <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                          <select class="form-control m-bot15" id="city" name="ecity" >
                                            <option value="">--Please Select Employee City</option>
                                            <?php
                                            if ($_SESSION['logged_in']['usertype']==1)
                                            {
                                            $employe = "SELECT `id`, `cityname` FROM `city` WHERE delid =0";
                                            $equery = mysqli_query($conn,$employe);

                                            while ($edata = mysqli_fetch_assoc($equery)) {
                                            echo "  <option value=".$edata['id'].">".$edata['cityname']."</option> ";
                                            }
                                          }
                                          else {
                                              $city = $_SESSION['logged_in']['usercity'];
                                            $employe = "SELECT `id`, `cityname` FROM `city` WHERE delid =0 and `id` = '$city'";
                                            $equery = mysqli_query($conn,$employe);

                                            while ($edata = mysqli_fetch_assoc($equery)) {
                                            echo "  <option value=".$edata['id'].">".$edata['cityname']."</option> ";
                                            }
                                          }
                                          ?>

                                        </select>
                                        </div>
                                    </div>

                                        <?php

                                        if ($_SESSION['logged_in']['usertype']==1)
                                        {
                                          echo '    <div class="form-group ">
                                                  <label for="etype" class="control-label col-lg-2"> Manager Name <span class="required">*</span></label>

                                                  <div class="col-lg-10">
                                            <select class="form-control m-bot15" id="managerid" name="managerid" >
                                              <option value="">--Please Select Employee Name</option>
                                            </select>
                                            </div>
                                        </div>';
                                          }
                                          else {
                                              echo '<input  hidden value="'.$_SESSION['logged_in']['id'].'" id="managerid" name="managerid" >';
                                          }?>


                                    <div class="form-group ">
                                        <label for="etype" class="control-label col-lg-2">Executive Name <span class="required">*</span></label>
                                        <div class="col-lg-10">
                                          <?php

                                          if ($_SESSION['logged_in']['usertype']==1)
                                          {
                                          echo '<select class="form-control m-bot15" id="executive" name="executive" >
                                            <option value="">--Please Select executive Name</option>';
                                          }else {
                                            $userid = $_SESSION['logged_in']['id'];
                                            echo '<select class="form-control m-bot15" id="executive" name="executive" >
                                              <option value="">--Please Select executive Name</option>';
                                            $city = $_SESSION['logged_in']['usercity'];
                                          $employe = "  SELECT `id`, `username` FROM `executive` WHERE delid = 0 AND managerid = '$userid'";
                                          $equery = mysqli_query($conn,$employe);

                                          while ($edata = mysqli_fetch_assoc($equery)) {
                                          echo "  <option value=".$edata['id'].">".$edata['username']."</option> ";
                                          }

                                          }
                                          ?>

                                        </select>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button  class="btn btn-primary" id="usersave" name="usersave" type="submit">Save</button>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php

                            if(isset($_POST['usersave']))
                            {

                              $executive = $_POST['executive'];
                              $userid = $_POST['userid'];
                              $claimno = $_POST['claimno'];
                              $mobileno = $_POST['mobileno'];
                              $description = $_POST['description'];
                              $ecity = $_POST['ecity'];
                              $etype = $_POST['managerid'];

                              $now = date('d/m/Y H:i:s');
                              $date = date('d/m/Y');


					                                 if($description=='' && $mobileno=='')
                                           {
                                             $insertuser = "INSERT INTO `newjob`(`date`, `claimno`, `description`, `branchid`, `managerid`, `savedby`, `savedon`, `employeid`, `mobile`, `delid`,`status`)
                                             VALUES('$date','$claimno','null','$ecity','$etype','$userid','$now','$executive','null',0,0)";
                                              $insertquery = mysqli_query($conn,$insertuser);

                                              if($insertquery)
                                              {
                                              $selctfcm = "SELECT `fcmid` FROM `executive` WHERE `id` = $executive";
                                                 $queryfcm = mysqli_query($conn,$selctfcm);
                                                 $data = mysqli_fetch_assoc($queryfcm);
                                                 $fcm = $data['fcmid'];

                                          //$notify =   notify($fcm);


                                                echo "<script>alert('Job Saved')</script>";
                                              //  header('location:reports.php');
                                              }
                                              else {
                                               echo "<script>alert('Fill form Properly')</script>";

                                              }
                                           }
                                           elseif ($description=='') {
                                             # code...
                                             $insertuser = "INSERT INTO `newjob`(`date`, `claimno`, `description`, `branchid`, `managerid`, `savedby`, `savedon`, `employeid`, `mobile`, `delid`,`status`)
                                              VALUES('$date','$claimno','null','$ecity','$etype','$userid','$now','$executive','$mobileno ',0,0)";
                                              $insertquery = mysqli_query($conn,$insertuser);

                                              if($insertquery)
                                              {
                                              $selctfcm = "SELECT `fcmid` FROM `executive` WHERE `id` = $executive";
                                                 $queryfcm = mysqli_query($conn,$selctfcm);
                                                 $data = mysqli_fetch_assoc($queryfcm);
                                                 $fcm = $data['fcmid'];

                                          //$notify =   notify($fcm);

                                                echo "<script>alert('Job Saved')</script>";
                                                //  header('location:reports.php');
                                              }
                                              else {
                                               echo "<script>alert('Fill form Properly')</script>";

                                              }
                                           }
                                           elseif ($mobileno=='') {
                                             # code...
                                             $insertuser = "INSERT INTO `newjob`(`date`, `claimno`, `description`, `branchid`, `managerid`, `savedby`, `savedon`, `employeid`, `mobile`, `delid`,`status`)
                                             VALUES('$date','$claimno','$description','$ecity','$etype','$userid','$now','$executive','null',0,0)";
                                              $insertquery = mysqli_query($conn,$insertuser);

                                              if($insertquery)
                                              {
                                              $selctfcm = "SELECT `fcmid` FROM `executive` WHERE `id` = $executive";
                                                 $queryfcm = mysqli_query($conn,$selctfcm);
                                                 $data = mysqli_fetch_assoc($queryfcm);
                                                 $fcm = $data['fcmid'];

                                              ////$notify =   notify($fcm);

                                                echo "<script>alert('Job Saved')</script>";
                                              //  header('location:reports.php');
                                              }
                                              else {
                                               echo "<script>alert('Fill form Properly')</script>";

                                              }
                                           }
                                           else {
                                             $insertuser = "INSERT INTO `newjob`(`date`, `claimno`, `description`, `branchid`, `managerid`, `savedby`, `savedon`, `employeid`, `mobile`, `delid`,`status`)
                                             VALUES('$date','$claimno','$description','$ecity','$etype','$userid','$now','$executive','$mobileno ',0,0)";
                                              $insertquery = mysqli_query($conn,$insertuser);

                                              if($insertquery)
                                              {
                                              $selctfcm = "SELECT `fcmid` FROM `executive` WHERE `id` = $executive";
                                                 $queryfcm = mysqli_query($conn,$selctfcm);
                                                 $data = mysqli_fetch_assoc($queryfcm);
                                                 $fcm = $data['fcmid'];

                                          //$notify =   notify($fcm);

                                                echo "<script>alert('Job Saved')</script>";
                                                //  header('location:reports.php');
                                              }
                                              else {
                                               echo "<script>alert('Fill form Properly')</script>";

                                              }
                                           }




                                }

                                function notify($fcm)
                                {
                                 $url = "https://fcm.googleapis.com/fcm/send";
                                    $headers = array(
                                      "authorization: key = AAAA5Lg-bJM:APA91bG6jObiDeAuRGizcSRFrs3yQuEEa41bowsBZQqrQXlb5yh5OgHflxmrKU6ro3haMLDQ5LgXJ9hNmEhYceGTVtdYH_npQJ9cdmWVjqRkjHAGawuOALIS-xaErKW6OrM4TknGkpEf",
                                      "cache-control: no-cache",
                                      "content-type: application/json",
                                      "postman-token: 91a0105a-2d7b-74f2-d146-92b49888dfbb"
                                    );
                                    $data2 = '{
                                    "registration_ids" : ["'.$fcm.'"],
                                    "notification" : {
                                    "body" : "New Job add",
                                    "title" : "New Job add",
                                    "icon" : "myicon"
                                    },
                                    "data" : {
                                    "body" : "New Job add",
                                    "title" : "New Job add"
                                    }
                                    }
                                    ';

                              $curl = curl_init();

                              curl_setopt_array($curl, array(
                                CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_POSTFIELDS =>$data2 ,
                                CURLOPT_HTTPHEADER => $headers
                              ));

                              $response = curl_exec($curl);
                              $err = curl_error($curl);

                              curl_close($curl);

                                }


                             ?>



                        </div>
                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
          <div class="text-right">
          <div class="credits">
                <!--
                    All the links in the footer should remain intact.
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
                -->
                <a href="#"> </a> Powered by <a href="http://www.illywhackertechnologies.com/">Illywhackertechnologies.com</a>
            </div>
        </div>
      </section>
      <!--main content end-->
  </section>

  <!-- container section start -->
  <?php
  include '../footer/add_job_footer.php';
   ?>
