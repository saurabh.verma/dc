<?php
ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '100M');
ini_set('max_input_time', 300);
ini_set('max_execution_time', 300);

include '../header.php'; ?>

  <body>
  <!-- container section start -->
  <section id="container" class="">


    <?php include '../topbar.php'; ?>
      <!--header end-->

      <!--sidebar start-->
    <?php include '../leftbar.php';
    ?>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content" style="overflow-y: scroll;">
        <section class="wrapper">
    <div class="row">
      <div class="col-lg-12">
          <h3 class="username"> Welcome <?php  print_r($_SESSION['logged_in']['username']); ?></h3>
        <h3 class="page-header"><i class="fa fa-table"></i> Area</h3>
        <ol class="breadcrumb">
          <li><i class="fa fa-home"></i><a href="index.html">Home</a></li>
          <li><i class="fa fa-table"></i>Area</li>
          <li><i class="fa fa-th-list"></i>Service  Area</li>
        </ol>
      </div>
    </div>
            <!-- page start-->

            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal-1" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Form Tittle</h4>
                                    </div>
                                    <div class="modal-body">

                                        <!-- <form role="form" method="post"> -->
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">City Name</label>
                                                <input type="email" class="form-control" id="cityname" name="cityname" placeholder="Enter City Name">
                                            </div>


                                            <button type="submit" class="btn btn-primary" id="csave" name="csave">Save</button>
                                        <!-- </form> -->
                                    </div>
                                </div>
                            </div>
                        </div>

            <div class="row">
                <div class="col-lg-12">
                  <!-- <div class="btn-group">
                              <button class="btn btn-warning btn-sm dropdown-toggle" style="float:center;margin-top: 20px;" type="submit" name="btn-download" id="btndownload" data-toggle="dropdown"><i class="fa fa-bars"></i>Download</button>
                                <ul class="dropdown-menu " role="menu">



      <li><a href="#" onClick ="$('#example').tableExport({type:'excel',escape:'false'});"> <img src='../assets/reportjs/xls.png' width='24px'> XLS</a></li>



    </ul>
                              </div> -->
                              <?php if ($_SESSION['logged_in']['usertype']==1) {
                              echo '  <a href="#myModal-1" data-toggle="modal" class="btn  btn-primary" style="  margin-left: 84px;  margin-top: 14px;">
                                          Add City
                                      </a>
                                      ';
                              } ?>
                    <section class="panel" id="newtable">


                        <header class="panel-heading">
                            Advanced Table
                        </header>

                        <table id="example" class="table">
  			<thead>
        <tr>
            <th>S.No</th>
          <th>Area</th>
            <th>Options</th>


        </tr>
      </thead>
      <tbody id="tBody">
        <?php
        $userlist = "SELECT * FROM `city` WHERE delid= 0 ";
        $userquery = mysqli_query($conn,$userlist);
        $i = 1;
        while ($data = mysqli_fetch_assoc($userquery)) {
        //  print_r($data);
          $id = $data['id'];
        echo '<tr>
          <td>'.$i.'</td>
          <td>'.$data['cityname'].'</td>
        <td> <a href="#"  class="delete"  data-id="'.$id.'" style="color: red;">Delete</a></td>
        </tr>';
        $i++;
        } ?>


      </tbody>
    </table>
  		                    </section>
                </div>
            </div>
            <!-- page end-->
        </section>
          <div class="text-right">
          <div class="credits">
                <!--
                    All the links in the footer should remain intact.
                    You can delete the links only if you purchased the pro version.
                    Licensing information: https://bootstrapmade.com/license/
                    Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
                -->
                <a href="#"> </a> Powered by <a href="http://www.illywhackertechnologies.com/">Illywhackertechnologies.com</a>
            </div>
        </div>
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->
  <?php
  include '../footer/area_footer.php'; ?>
>
